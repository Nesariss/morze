def get_supplementary():
    return {
    'A': '.-', 
    'B': '-...', 'C': '-.-.',
    'D': '-..', 'E': '.',
    'F': '..-.', 'G': '--.',
    'H': '....', 'I': '..', 'J': '.---',
    'K': '-.-', 'L': '.-..', 'M': '--',
    'N': '-.', 'O': '---', 'P': '.--.',
    'Q': '--.-', 'R': '.-.', 'S': '...',
    'T': '-', 'U': '..-', 'V': '...-',
    'W': '.--', 'X': '-..-', 'Y': '-.--', 
    'Z': '--..', '1': '.----', '2': '..---',
    '3': '...--', '4': '....-', '5': '.....',
    '6': '-....', '7': '--...', '8': '---..',
    '9': '----.', '0': '-----', ', ': '--..--',
    '.': '.-.-.-', '?': '..--..',
    '/': '-..-.', '-': '-....-', '(': '-.--.', ')': '-.--.-'
    } 


def code_morze(value):
    '''
    please add your solution here or call your solution implemented in different function from here
    then change return value from 'False' to value that will be returned by your solution
    '''
    result = []
    supplementary = get_supplementary()
    split_letters = value.replace(" ", "").upper()
    for letter in split_letters:
        encoded = supplementary.get(letter, "<no value>")
        result.append(encoded)
    result = " ".join(result)
    return result